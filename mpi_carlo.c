//
// Created by erfan on 10/22/16.
//
#include <mpi.h>
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <math.h>


int main(int argc, char** argv) {
    // Initialize the MPI environment
    MPI_Init(NULL, NULL);
    int N = 100000000;
    int n = 0;
    int o = 0;
    int sum_n = 0;
    int world_size, world_rank, begin, end;
    double x_number, y_number;

    // Get the number of processes
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);

    // Get the rank of the process
    MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);

    MPI_Barrier(MPI_COMM_WORLD);
    begin = MPI_Wtime();

    srand((unsigned int) time(0));

    for(int i = 0;i< (N/world_size);i++){
        x_number = (double)rand() / ((double)RAND_MAX+1);
        y_number = (double)rand() / ((double)RAND_MAX+1);
        if(sqrt((x_number*x_number) + (y_number*y_number)) < 1)
            n+= 1;
        else
            o++;
    }

    MPI_Reduce(&n, &sum_n, 1, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);

    MPI_Barrier(MPI_COMM_WORLD);
    end = MPI_Wtime();
    if(world_rank==0){
        printf("Pi = %f\n", 4.0 * ( double)sum_n / (double) N);
        printf("Elapsed time = %d\n", end - begin);
    }

    // Finalize the MPI environment.
    MPI_Finalize();
}


