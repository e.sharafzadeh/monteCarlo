#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <math.h>

int main( int argc, char *argv[]) {
    printf("Hello Monte Carlo!\n");
    int N = atoi(argv[1]);
    double n = 0;
    double x_number;
    double y_number;
    srand((unsigned int) time(NULL));
    for(int i=0;i< N;i++){
        x_number = (double)rand() / (double)RAND_MAX;
        y_number = (double)rand() / (double)RAND_MAX;
        if(sqrt((x_number*x_number) + (y_number*y_number)) < 1)
            n+= 1;
    }
    printf("Pi = %f\n", 4.0 * n / (double) N);



    return 0;
}